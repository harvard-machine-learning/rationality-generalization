import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models.resnet import resnet50
import sys
sys.path.insert(0, './models')
from basicmodels import create_fc, create_cnn, Flatten
from resnet_pytorch_image_classification import resnet_pic
from wrn import wideresnet_pic, wideresnet_mine
from resnet import resnet18k_cifar

class Model(nn.Module):
    def __init__(self, feature_dim=128, settings=None):
        super(Model, self).__init__()

        No = 10 #This doesnt really matter
        if settings.classifiername=='mCNN_k_cifar10':
            resolution = 32
            d_output = No
            c_in = 3
            c_first = settings.conv_channels
            n_layers = 5
            classifier = create_cnn(resolution, d_output, c_in, c_first, n_layers, batch_norm=False)

            h_width = 16*settings.conv_channels

            self.f = []
            for i in range(len(classifier)):
                if not isinstance(classifier[i], nn.Linear):
                    self.f.append(classifier[i])            

        elif settings.classifiername=='mCNN_k_bn_cifar10':
            resolution = 32
            d_output = No
            c_in = 3
            c_first = settings.conv_channels
            n_layers = 5
            classifier = create_cnn(resolution, d_output, c_in, c_first, n_layers, batch_norm=True)

            h_width = 16*settings.conv_channels

            self.f = []
            for i in range(len(classifier)):
                if not isinstance(classifier[i], nn.Linear):
                    self.f.append(classifier[i])

        elif settings.classifiername=='resnet18k_cifar':
            k = settings.conv_channels
            num_classes = No
            classifier = resnet18k_cifar(k, num_classes)

            h_width = 16*8*settings.conv_channels

            self.f = []
            for name, module in classifier.named_children():
                if not isinstance(module, nn.Linear):
                    self.f.append(module)            

        elif settings.classifiername=='resnet50':
            classifier = resnet50()

            self.f = []
            for name, module in classifier.named_children():
                if name == 'conv1':
                    module = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False)
                if not isinstance(module, nn.Linear) and not isinstance(module, nn.MaxPool2d):
                    self.f.append(module)

            h_width = 2048

        elif settings.classifiername=='fc_samewidth':

            depth = settings.depth
            numhid = h_width = settings.depth
            nonlinearity = nn.ReLU()
            in_size = 32*32*3
            
            layers = [Flatten(), nn.Linear(in_size, numhid, bias=True)]
            layers += [nn.BatchNorm1d(numhid)]
            layers += [nonlinearity]

            for d in range(depth-2):
                layers += [nn.Linear(numhid, numhid, bias=True)]
                layers += [nn.BatchNorm1d(numhid)]
                layers += [nonlinearity]
            layers += [nn.Linear(numhid, numhid, bias=True)]
            layers += [nn.BatchNorm1d(numhid)]
            self.f = layers
                    
        else:
            raise NotImplementedError

        print(h_width)
        self.h_width = h_width

        # if settings.classifiername!='resnet50':
        #     self.f = []
        #     for name, module in classifier.children():
        #         if not isinstance(module, nn.Linear):
        #             self.f.append(module)

            
        
        # for name, module in resnet50().named_children():
        #     if name == 'conv1':
        #         module = nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False)
        #     if not isinstance(module, nn.Linear) and not isinstance(module, nn.MaxPool2d):
        #         self.f.append(module)
        # encoder
        self.f = nn.Sequential(*self.f)
        # projection head
        self.g = nn.Sequential(nn.Linear(h_width, 512, bias=False), nn.BatchNorm1d(512),
                               nn.ReLU(inplace=True), nn.Linear(512, feature_dim, bias=True))

    def forward(self, x):
        x = self.f(x)
        feature = torch.flatten(x, start_dim=1)
        out = self.g(feature)
        return F.normalize(feature, dim=-1), F.normalize(out, dim=-1)
