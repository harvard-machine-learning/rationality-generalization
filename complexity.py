import numpy as np
import torch


def entropy(ps):
    '''Compute entropy.'''
    ps.flatten()
    ps = ps[np.nonzero(ps)]            # toss out zeros
    H = -np.sum(ps * np.log2(ps))   # compute entropy
    
    return H


def mi(x, y, bins=1000):
    '''
    Compute mutual information
    NOTE: density is False because it is buggy.
    '''
    p_xy = np.histogram2d(x, y, bins=[bins, bins], density=False)[0] #returns the mutual density p(x,y)
    p_x  = np.histogram(  x,    bins=bins,         density=False)[0] #returns the marginal density p(x)
    p_y  = np.histogram(  y,    bins=bins,         density=False)[0] #returns the marginal density p()
    
    p_xy = p_xy/p_xy.sum()
    p_x = p_x/p_x.sum()
    p_y = p_y/p_y.sum()

    
    H_xy = entropy(p_xy)
    H_x  = entropy(p_x)
    H_y  = entropy(p_y)
    
    return H_x + H_y - H_xy


def density(vec_list):
    assert type(vec_list) is list or len(vec_list.shape) > 1
    ids, counts = np.unique(np.concatenate([vec.reshape(-1, 1) for vec in vec_list], axis=1), axis=0,
                            return_counts=True)
    return counts / np.sum(counts)


def con_mi(x, y, z):
    p_xyz = density([x, y, z])
    p_xz = density([x, z])
    p_yz = density([y, z])
    p_z = density([z])

    return entropy(p_xz) + entropy(p_yz) - entropy(p_xyz) - entropy(p_z)


def predict(model, x_train, bs=2048):
    n = len(x_train)
    preds = np.zeros(n, dtype=int)
    model.eval()

    with torch.no_grad():

        for i in range((n-1)//bs + 1):
            x_batch = x_train[i*bs: (i+1)*bs]

            outputs = model((x_batch).cuda()).cpu().numpy()
            preds[i*bs: (i+1)*bs] = np.argmax(outputs, axis=1)

    return preds


def estimate_complexity(model, train_data, num_classes):
    noise_vector = (np.array(train_data.targets) - train_data.clean_targets.numpy()) % num_classes  # 0 if there was noise value of noise o.w.
    preds = predict(model, train_data.features)
    return mi(preds, noise_vector, bins=num_classes)


def estimate_complexity_new(model, train_data, num_classes):
#    noise_vector = (np.array(train_data.targets) - train_data.clean_targets.numpy()) % num_classes  # 0 if there was noise value of noise o.w.    
    preds = predict(model, train_data.features)
    return con_mi(preds, np.array(train_data.targets), train_data.clean_targets.numpy()) # return I(g(x); N | y)
