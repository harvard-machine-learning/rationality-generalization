
def l1_regularization(model, mu=1.):
    regularization = 0.
    for param in model.parameters():
        regularization += param.abs().sum()
    return mu * regularization
