import argparse, os
import torch
import torch.nn as nn
import torch.optim as optim
from thop import profile, clever_format
from torch.utils.data import DataLoader, SubsetRandomSampler
from torchvision.datasets import CIFAR10
from tqdm import tqdm

import numpy as np
import numpy.random as npr

from data_settings import get_dataset
from model_settings import Net
from optims_lr import get_optimizers, get_lr_scheduler
from model import Model
from regularizarion import l1_regularization
import wandb
from torch.utils.data import Dataset

import copy
from complexity import estimate_complexity, estimate_complexity_new, predict


# train or test for one epoch
def train_val(net, data_loader, train_optimizer, l1_weight=0):
    is_train = train_optimizer is not None
    net.train() if is_train else net.eval()

    total_loss, total_correct_1, total_correct_5, total_correct_1_clean, total_correct_5_clean, total_num, data_bar = 0.0, 0.0, 0.0, 0.0, 0.0, 0, tqdm(data_loader)
    with (torch.enable_grad() if is_train else torch.no_grad()):
        for data, target, clean_target in data_bar:
            data, target, clean_target = data.cuda(non_blocking=True), target.cuda(non_blocking=True), clean_target.cuda(non_blocking=True)
            out = net(data)

            loss = loss_criterion(out, target)
            if l1_weight != 0 and is_train:
                loss += l1_regularization(net.eval_net, l1_weight)

            if is_train:
                train_optimizer.zero_grad()
                loss.backward()
                train_optimizer.step()

            total_num += data.size(0)
            total_loss += loss.item() * data.size(0)
            prediction = torch.argsort(out, dim=-1, descending=True)

            ## Noisy lables
            total_correct_1 += torch.sum((prediction[:, 0:1] == target.unsqueeze(dim=-1)).any(dim=-1).float()).item()
            total_correct_5 += torch.sum((prediction[:, 0:5] == target.unsqueeze(dim=-1)).any(dim=-1).float()).item()

            ## Clean labels

            total_correct_1_clean += torch.sum((prediction[:, 0:1] == clean_target.unsqueeze(dim=-1)).any(dim=-1).float()).item()
            total_correct_5_clean += torch.sum((prediction[:, 0:5] == clean_target.unsqueeze(dim=-1)).any(dim=-1).float()).item()

            data_bar.set_description('{} Epoch: [{}/{}] Loss: {:.4f} ACC@1: {:.2f}% ACC@5: {:.2f}%'
                                     .format('Train' if is_train else 'Test', epoch, epochs, total_loss / total_num,
                                             total_correct_1 / total_num * 100, total_correct_5 / total_num * 100))

    return total_loss / total_num, total_correct_1 / total_num * 100, total_correct_5 / total_num * 100, total_correct_1_clean / total_num * 100, total_correct_5_clean / total_num * 100


def generate_noise(clean_labels, pnoise):
    assert 0 <= pnoise <= 1
    if pnoise == 0.:
        return clean_labels, None

    n = len(clean_labels)
    num_noise = int(pnoise * n)
    num_classes = clean_labels.max() + 1
    noise_ind = np.random.permutation(n)[: num_noise]
    noise     = 1 + np.random.choice(num_classes - 2, num_noise) # want noise to be 1,...,999 = 1 + 0,...,998

    noised_labels = clean_labels.copy()
    noised_labels[noise_ind] = (noised_labels[noise_ind] + noise) % num_classes
    return noised_labels, noise_ind


def download_dataset(args):
    path = args.feature_path
    while path.endswith('/'):
        path = path[:-1]
    destination = f'data/tmp/{os.path.basename(path)}'
    os.makedirs(destination, exist_ok=True)
    if not args.use_local:
        os.system(f'gsutil -m cp -r {path}/ {destination}')
    args.feature_path = destination


def get_dataset_metadata_and_labels(args):
    from glob import glob
    fpaths = [path for path in glob(args.feature_path + '/*')]

    num_classes = (args.dataname == 'CIFAR10')*10 + (args.dataname == 'CIFAR100')*100 + (args.dataname == 'ImageNet')*1000
    filter_with_labels = [fpath for fpath in fpaths if 'train_labels.npy' in fpath]
    assert len(filter_with_labels) == 1
    label_path = filter_with_labels[0]
    clean_labels = np.load(label_path)

    features_paths = [fpath for fpath in fpaths if 'aug_train_features' in fpath]
    num_features = len(features_paths)

    noised_labels_and_locations = [generate_noise(clean_labels, args.train_noise_prob) for _ in range(num_features)]

    features_path_dict = {
        i: features_paths[i] for i in range(num_features)
    }
    noised_labels_dict = {
        i: noised_labels_and_locations[i][0] for i in range(num_features)
    }
    noised_locations_dict = {
        i: noised_labels_and_locations[i][1] for i in range(num_features)
    }
    first_feature = np.load(features_path_dict[0])

    test_features_path = [fpath for fpath in fpaths if 'test_features.npy' in fpath][0]
    test_labels_path   = [fpath for fpath in fpaths if 'test_labels.npy' in fpath][0]

    test_tensors = torch.from_numpy(np.load(test_features_path)), torch.from_numpy(np.load(test_labels_path))

    return num_classes, first_feature, features_path_dict, noised_labels_dict, noised_locations_dict, test_tensors, clean_labels

def eval_augmented_testdata(args, model):
    from glob import glob
    fpaths = [path for path in glob(args.feature_path + '/*')]

    test_features_paths = [fpath for fpath in fpaths if 'aug_test_features' in fpath]
    test_labels_paths = [fpath for fpath in fpaths if 'aug_test_labels' in fpath]
    num_test_features = len(test_features_paths)
    print(f'Number of test sets = {num_test_features}')

    aug_test_accs = []
    for i, (feature_path, label_path) in enumerate(zip(test_features_paths, test_labels_paths)):
        test_tensors = torch.from_numpy(np.load(feature_path)), torch.from_numpy(np.load(label_path))
        test_loader = get_test_loaders(*test_tensors)
        test_loss, test_acc_1, test_acc_5, _, _ = train_val(model, test_loader, None)        
        aug_test_accs += [test_acc_1]

    wandb.run.summary["First Aug Test Acc"] = aug_test_accs[0]
    wandb.run.summary["Ten Aug Test Acc"] = np.mean(aug_test_accs)
    wandb.run.summary.update()

    

def get_noise(epoch, noised_labels_dict, noised_locations_dict):
    num_augs = len(features_path_dict)
    return noised_labels_dict[epoch % num_augs], noised_locations_dict[epoch % num_augs]


def get_features(epoch, features_path_dict):
    num_augs = len(features_path_dict)
    return np.load(features_path_dict[epoch % num_augs])


class trained_features(Dataset):
    '''
    Make a dataset from the trained features
    '''

    def __init__(self, features, labels, clean_labels):
        self.features     = features
        self.labels       = labels
        self.clean_labels = clean_labels

    def __len__(self):
        return len(self.clean_labels)

    def __getitem__(self, idx):
        return self.features[idx], self.labels[idx], self.clean_labels[idx]


def get_test_loaders(features, labels):
    test_data = trained_features(features,
                                  labels,
                                  labels)
    return DataLoader(test_data, batch_size=batch_size, shuffle=True, num_workers=16, pin_memory=True)


def get_curr_loaders(curr_features, curr_noised_labels, curr_noise_locs, clean_labels):
    train_data = trained_features(torch.from_numpy(curr_features),
                                  torch.from_numpy(curr_noised_labels),
                                  torch.from_numpy(clean_labels))

    train_loader       = DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=16, pin_memory=True)
    train_noisy_loader = DataLoader(train_data, batch_size=batch_size, sampler=SubsetRandomSampler(curr_noise_locs),
                                    shuffle=False, num_workers=16, pin_memory=True)
    return train_loader, train_noisy_loader


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Evaluation')
    ### Feature options
    parser.add_argument('--from_features', action='store_true')
    parser.add_argument('--feature_path', type=str, help='Path to trained features')
    parser.add_argument('--local_path', type=str, help='Path to download trained features if GCS')
    parser.add_argument('--model_path', type=str, help='Path to pre-trained model')
    parser.add_argument('--dataname', required=True, type=str, help='CIFAR10/CIFAR100/ImageNet')

    parser.add_argument('--complexity_cmi', action='store_true')

    ### Training options
    parser.add_argument('--batch_size', type=int, default=512, help='Number of images in each mini-batch')
    parser.add_argument('--epochs', type=int, default=100, help='Number of sweeps over the dataset to train')
    
    # Optimizer
    parser.add_argument('--optimname', type=str, help='sgd, momentum, adam')
    parser.add_argument('--momentum', type=float, help='Momentum for SGD+momentum')
    parser.add_argument('--nesterov', action='store_true')
    parser.add_argument('--beta1',default=0.9, type=float, help='beta1 for Adam')
    parser.add_argument('--beta2', default=0.999, type=float, help='beta2 for Adam')

    # Learning rate
    parser.add_argument('--lr_sched_type', help='const, sqrtT')
    parser.add_argument('--lr', type=float, help='const, sqrtT LR: init LR value')
    parser.add_argument('--lr_gamma', type=float, help='Learning rate drops as 1/(T)**lr_gamma')
    parser.add_argument('--lr_drop_factor', type=float, help='decay: Factor to multiply LR by at for MultiStepLR')
    parser.add_argument('--lr_epoch_list', type=int, default = 0, nargs='*', help='at_epoch: Epochs to change the learning rate to value from previous argument')

    ### Evaluation options
    parser.add_argument('--eval_loss', default='ce', help="MSE/CE")
    parser.add_argument('--train_noise_prob', default=0., type=float, help='Noise added to labels')
    parser.add_argument('--eval_type', type=str, help='linear, fc')
    parser.add_argument('--weight_decay', type=float, help='L2 regularization value', default=0.)
    parser.add_argument('--l1_regularization', type=float, help='weight of L1 regularization', default=0.)

    # Non-linear evaluation options
    parser.add_argument('--eval_arch', type=str, help='Specify a predefined architecture type')
    parser.add_argument('--eval_nl_width', type=int, help='Eval FC network width - same for all layers')
    parser.add_argument('--eval_nl_depth', type=int, help='Eval FC network depth')
    parser.add_argument('--eval_nl_bn', action='store_true', help='Add BN to non-linear eval network')

    # wandb settings
    parser.add_argument('--log_predictions', action='store_true')
    parser.add_argument('--wandb_project_name', type=str)
    parser.add_argument('--savepath', type=str)
    parser.add_argument('--use_local', action='store_true')

    args = parser.parse_args()
    print(args)

    ######## Initial setup
    # Wandb
    wandb_path = os.path.join(args.wandb_api_key_path)

    with open(wandb_path, 'r') as f:
        key = f.read().split('\n')[0]

    wandb.init(project=args.wandb_project_name)
    wandb.config.update(args)
    wandb_run_id = wandb.run.get_url().split('/')[-1]

    # Data storage
    results_dir = os.path.join(args.savepath, args.wandb_project_name, wandb_run_id)
    try:
        os.makedirs(results_dir)
    except OSError:
        pass

    batch_size, epochs = args.batch_size, args.epochs    

    ########################################################################
    ### Download features, get meta data, clean and dirty label ############
    ########################################################################
    if not args.use_local: download_dataset(args)
    num_classes, curr_features, features_path_dict, noised_labels_dict, noised_locations_dict, \
        test_tensors, clean_labels = get_dataset_metadata_and_labels(args)

    test_loader = get_test_loaders(*test_tensors)
    ####### Model
    model = Net(num_class=num_classes, args=args, feature_size=curr_features.shape[1]).cuda()
    print(model)

    ###### Optimizer + LR schedule
    assert args.weight_decay == 0 or args.l1_regularization == 0, "We probably don't want both l1 and l2 regularization"
    optimizer = get_optimizers(args, model.eval_net) #initial_lr = 1.
    lr_scheduler = get_lr_scheduler(args, optimizer)

    for param_group in optimizer.param_groups:
        if args.weight_decay is not None: param_group['weight_decay'] = args.weight_decay

    ##### Loss
    if args.eval_loss == 'ce':
        loss_criterion = nn.CrossEntropyLoss()
    elif args.eval_loss == 'mse':
        lossfn = nn.MSELoss()
        loss_criterion = lambda prediction, target: lossfn(prediction, torch.eye(num_classes, device=target.device)[target])
    else:
        raise NotImplementedError

    best_acc = 0.0
    for epoch in range(0, epochs):
        curr_noised_labels, curr_noise_locs = get_noise(epoch, noised_labels_dict, noised_locations_dict)
        if epoch != 0:
            del train_loader, train_noisy_loader
            curr_features = get_features(epoch, features_path_dict)
        train_loader, train_noisy_loader = get_curr_loaders(curr_features, curr_noised_labels,
                                                            curr_noise_locs, clean_labels)


        train_loss, train_acc_1, train_acc_5, train_clean_acc_1, train_clean_acc_5 = train_val(model,
                                                                                               train_loader,
                                                                                               optimizer,
                                                                                               args.l1_regularization)
        lr_scheduler.step()
        test_loss, test_acc_1, test_acc_5, _, _ = train_val(model, test_loader, None)
        if args.train_noise_prob > 0.:        
            train_noisy_loss, _, _, train_noisy_acc_1, train_noisy_acc_5 = train_val(model, train_noisy_loader, None)
        if test_acc_1 > best_acc:
            best_acc = test_acc_1
            print(os.path.join(results_dir, 'eval_model.pt'))
            try:
                torch.save(model.state_dict(), os.path.join(results_dir, 'eval_model.pt'))
            except:
                pass

        metrics = {}
        metrics['Linear Train Loss'] = train_loss
        metrics['Linear Test Loss'] = test_loss

        metrics['Linear Test Acc @ 1'] = test_acc_1    
        metrics['Linear Train Acc @ 1'] = train_acc_1
        metrics['Linear Train Acc @ 1 - Clean'] = train_clean_acc_1

        if args.train_noise_prob > 0.:
            metrics['Linear Noisy Acc @ 1 - Clean'] = train_noisy_acc_1

        wandb.log(metrics)

    ##### Evaluate augmented test
    eval_augmented_testdata(args, model)

    ##### Log predictions
    if args.log_predictions:
        for i in range(len(features_path_dict)):
            preds = predict(model, torch.from_numpy(get_features(i, features_path_dict)))
            curr_noised_labels, _ = get_noise(i, noised_labels_dict, noised_locations_dict)

           wandb.run.summary[f"Predictions_{i}"] = preds
           wandb.run.summary[f"Noisy Labels_{i}"] = curr_noised_labels

        wandb.run.summary["Clean Labels"] = clean_labels

        wandb.run.summary.update()
